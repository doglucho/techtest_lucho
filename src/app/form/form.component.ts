import { Component, OnInit, ElementRef, Input } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @Input('email') 
  email!: string | boolean;
  @Input('checkbox')
  checkbox!: boolean
  @Input('residence')
  residence!: ElementRef;
  @Input('submitButton')
  submitButton!: boolean;
  
  constructor() { }
  
  ngAfterViewInit(): void {
  }
  
  ngOnInit(): void {
  }

  toggleButton(): any {
    this.email = (document.getElementById('email') as HTMLInputElement)!.checkValidity()
    this.residence = (document.getElementById('residence') as HTMLFormElement)!['value']
    this.checkbox = (document.getElementById('checkbox') as HTMLInputElement)!.checked

    if(this.email && this.residence && this.checkbox) {
        (document.getElementById('submitButton') as HTMLButtonElement)!.disabled = false
    } else {
        (document.getElementById('submitButton') as HTMLButtonElement)!.disabled = true
    }
  }

  showMessage(): any {
    window.onload = () => {
      (document.getElementById('submitButton') as HTMLElement).onclick = this.showMessage
    }
    return alert('¡Perfecto, te avisaremos la primera!')
  }
}