## Techtest_Lucho

This is a technical proof requested by Vicente containing the following description:

## Thriflora

Thriflora es un empresa que se dedica a suministrar plantas a viveros de todo el mundo desde el año 1988.
Desde el principio, siempre se ha dedicado a la venta a empresas, pero desde hace tiempo están pensando en hacer venta directa online a cualquier persona.

## Problema

El equipo de desarrollo ya está trabajando en montar una tienda online pero todavía quedan un par de meses hasta que este disponible. Mientras tanto el equipo de marketing ha pensado que sería interesante hacer una página para poder ir recogiendo los emails de potenciales clientes que quieran ser los primeros en probar la tienda online, que por el momento solo se va abrir en España, Francia e Italia.

## Solución

Los equipos de desarrollo y marketing han acordado que van a montar una web para que los clientes potenciales pueden dejar sus datos.
El equipo de marketing quiere que los datos se registren en un servicio externo, pero todavía no han decidido cual, así que han pedido al equipo de desarrollo que de momento se centren solo en la web con el formulario y que se despreocupen de momento de meter ningún tipo de base de datos.

## Requisitos

- Los usuarios introducen su email en un campo de texto.
- Los usuarios seleccionan su país de residencia en un selector.
- El selector solo muestra los paises donde se va a abrir la tienda: España, Francia e Italia.
- Los usuarios aceptan recibir el email usando un checkbox "Quiero recibir un email cuando la tienda esté disponible".
- Los usuarios se registran haciendo click en el botón "¡Quiero ser la primera en comprar!".
- El botón para apuntarse solo se habilita cuando el email es válido, hay un país seleccionado y el checkbox está marcado.
- Cuando el usuario se ha registrado correctamente, aparece un mensaje de registro completado: "¡Perfecto, te avisaremos la primera!"

# Commands

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `npm test` to execute the unit tests via Jest.

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
